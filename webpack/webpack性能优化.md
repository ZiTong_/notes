webpack-bundle-analyzer查看包的体积
speed-measure-webpack-plugin 查看打包的速度

1.treeshake 消除无用的js代码
>1. 代码不会被执行，不可到达
>2. 代码执行的结果不会被用到
>3. 代码只会影响死变量（只写不读）

2.动态导入polyfill支持低版本的浏览器  
3.使用cdn externals 字段  
4.使用mini-css压缩css   
5.使用terser-webpack 压缩js parallel 开启多进程压缩   
6.优化包的体积,例如lodash、moment、ant按需加载  
7.图片压缩  
8.happack 开启多进程加速打包  
9.使用splitchunks抽取公共模块 
8.利用includes、excludes加快搜索文件  
9.使用 resolve 字段告诉 webpack 怎么去搜索文件。 使用 resolve.alias 减少查找过程  
10.code spilt 代码分片  
11.擦除无用的 CSS purgeCSS  
12.预先编译资源模块（DllPlugin）  
13.合理使用 sourceMap   
14。babel-loader 开启缓存 terser-webpack-plugin 开启缓存 使用 cache-loader 或者 hard-source-webpack-plugin  
15.Prefetch 预加载资源 webpackPrefetch使用 webpackPrefetch 来提前预加载一些资源，意思就是 将来可能需要一些模块资源，在核心代码加载完成之后带宽空闲的时候再去加载需要用到的模块代码。