let instance = null

class Storage {
  static getInstance(){
    if(!instance){
      instance = new Storage()
    }
    return this.instance
  }
  setItem = (key, val) => localStorage.setItem(key, val)
  getItem = (key) => localStorage.getItem(key)
}