//! 单向链表
//! 节点 
class Node{
  constructor(element){
    this.element = element
    this.next = null   // null  表示最后一个节点
  }
}

//! 链表
class LinkedList{
  constructor(){
    this.head = null
    this.count = 0
  }

  push(element){
    const node = new Node(element)
    let currentNode = null
    if(this.head){
      currentNode = this.head
      while(currentNode.next){
        currentNode = currentNode.next
      }
      currentNode.next = node
    }else{
      this.head = node
    }
    this.count++
  }

  reverse(){
    let preNode = null
    let nextNode = null
    while(this.head){
      nextNode = this.head.next
      this.head.next = preNode
      preNode = this.head
      this.head = nextNode
    }
  }
}

const list = new LinkedList()
list.push(1)
list.push(2)
list.push(3)
list.push(4)
list.reverse()
console.log('LinkedList', list)    // { head: Node, count: 4 }