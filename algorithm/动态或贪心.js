/*
给定一个数组，它的第 i 个元素是一支给定股票第 i 天的价格。
如果你最多只允许完成一笔交易（即买入和卖出一支股票），设计一个算法来计算你所能获取的最大利润。
注意你不能在买入股票前卖出股票。

示例 1:

输入: [7,1,5,3,6,4]
输出: 5
解释: 在第 2 天（股票价格 = 1）的时候买入，在第 5 天（股票价格 = 6）的时候卖出，最大利润 = 6-1 = 5 。
     注意利润不能是 7-1 = 6, 因为卖出价格需要大于买入价格。
示例 2:

输入: [7,6,4,3,1]
输出: 0
解释: 在这种情况下, 没有交易完成, 所以最大利润为 0。
*/

//  第一题：定义一个Queue类，第一次调用可以隔1000秒输出1，，第二次调用可以隔2000秒输出2...最后可以调用run();
class Queue {
  constructor() {
    this.taskCallback = []
  }

  task(fn, time) {
    let _this = this
    this.taskCallback.push(
      setTimeout(() => {
        fn.call(_this)
      }, time)
    )
    return this
  }

  run() {
    this.taskCallback.map(t => {
      t
    })
  }
}

// new Queue()
//   .task(() => {
//     console.log(1)
//   }, 1000).task(() => {
//     console.log(2)
//   }, 2000).task(() => {
//     console.log(3)
//   }, 3000).run()

// 第二题：定义一个函数，求和函数，可以无数次调用，并且在最后调用valueOf显示最终求和结果；
// sum(1)(2)(3)(4).valueOf()

const sum = (...num) => {
  const cur = (...count) => {
    return sum.apply(this, num.concat(count))
  }

  cur.valueOf = () => {
    return num.reduce((pre, cur) => pre + cur)
  }
  return cur
}

// console.log(sum(1)(2)(3)(4).valueOf())

// 给定 n 个非负整数 a1，a2，...，an，每个数代表坐标中的一个点 (i, ai) 。在坐标内画 n 条垂直线，垂直线 i 的两个端点分别为 (i, ai) 和 (i, 0)。找出其中的两条线，使得它们与 x 轴共同构成的容器可以容纳最多的水。
// 说明：你不能倾斜容器，且 n 的值至少为 2。

// 输入: [1,8,6,2,5,4,8,3,7]
// 输出: 49
// (1, 0)(1, 1)(2, 0)(2, 8)(3, 0)(3, 6)
const maxArea = (arr) => {
  const len = arr.length - 1
  let i = 0, j = len, max = 0;
  while (i !== j) {
    let height = 0
    if (arr[len] > arr[i]) {
      height = arr[len]
      i++
    } else {
      height = arr[i]
      j--
    }
    const width = j - i
    max = width * height > max ? width * height : max
  }
  return max
}

console.log(maxArea([1, 8, 6, 2, 5, 4, 8, 3, 7]))