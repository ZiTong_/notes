import * as React from "react"

const initialState: { count: number } = {
  count: 0
}

const reducer = (state, action) => {
  switch (action.type) {
    case "increment":
      return { count: state.count + action.payload }
    case "decrement":
      return { count: state.count - action.payload }
    default:
      throw new Error()
  }
}

const App = () => {
  const [state, dispatch] = React.useReducer(reducer, initialState)

  return (
    <>
      Count: {state.count}
      <button onClick={() => dispatch({ type: "increment", payload: 5 })}>+</button>
      <button onClick={() => dispatch({ type: "decrement", payload: 5 })}>-</button>
    </>
  )
}

// mini版 redux
