import * as React from "react"

let timer = null
const App = () => {
  const [count, setCount] = React.useState(0)

  React.useEffect(() => {
    document.title = "componentDidMount" + count
  }, [count])

  React.useEffect(() => {
    timer = setInterval(() => {
      setCount(prevCount => prevCount + 1)
    }, 1000)
    // 一定注意下这个顺序：
    // 告诉react在下次重新渲染组件之后，同时是下次执行上面setInterval之前调用
    return () => {
      document.title = "componentWillUnmount"
      clearInterval(timer)
    }
  }, [])

  return (
    <div>
      Count: {count}
      <button onClick={() => clearInterval(timer)}>clear</button>
    </div>
  );
}

// 1.比如第一个 useEffect 中，理解起来就是一旦 count 值发生改变，则修改 documen.title 值；
// 2.而第二个 useEffect 中传递了一个空数组[]，这种情况下只有在组件初始化或销毁的时候才会触发，用来代替 componentDidMount 和 componentWillUnmount，慎用；
// 3.还有另外一个情况，就是不传递第二个参数，也就是useEffect只接收了第一个函数参数，代表不监听任何参数变化。每次渲染DOM之后，都会执行useEffect中的函数。