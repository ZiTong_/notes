import * as React from "react"

const colorContext = React.createContext("gray")

const Bar = () => {
	const color = React.useContext(colorContext)
	return <div>{ color }</div>
}

const Foo = () => <Bar />

const App = () => {
	return (
		<colorContext.Provider value={"red"}>
			<Foo/>
		</colorContext.Provider>
	)
}

//useContext 可以解决 Consumer 多状态嵌套的问题