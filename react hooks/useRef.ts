import * as React from "react"

const App = () => {
  let [name, setName] = React.useState("Nate")
  let nameRef = React.useRef()

  const submitButton = () => {
    setName(nameRef.current.value)
  }

  return (
    <div className="App">
      <p>{name}</p>
      <div>
        <input ref={nameRef} type="text" />
        <button type="button" onClick={submitButton}>
          Submit
        </button>
      </div>
    </div>
  )
}

//useRef 跟 createRef 类似，都可以用来生成对 DOM 对象的引用
//useRef 返回的值传递给组件或者 DOM 的 ref 属性，
//就可以通过 ref.current 值访问组件或真实的 DOM 节点，重点是组件也是可以访问到的


//React Hooks 中存在 Capture Value

consrt App = ()  => {
  const [count, setCount] = React.useState(0)

  React.useEffect(() => {
    setTimeout(() => {
      alert("count: " + count)
    }, 3000)
  }, [count])

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>增加 count</button>
      <button onClick={() => setCount(count - 1)}>减少 count</button>
    </div>
  )
}
// 点击+，-后函数组件返回1,0,而不是0,0
// 类组件会直接获取引用最新的值0

const App = () => {

  const count = React.useRef(0)

  const showCount = () => {
    alert("count: " + count.current)
  }

  const handleClick = number => {
    count.current = count.current + number
    setTimeout(showCount, 3000)
  }

  return (
    <div>
      <p>You clicked {count.current} times</p>
      <button onClick={() => handleClick(1)}>增加count</button>
      <button onClick={() => handleClick(-1)}>减少count</button>
    </div>
  )
}
//只要将赋值与取值的对象变成 useRef，而不是 useState，就可以躲过 capture value 特性，在 3 秒后得到最新的值。
//seRef 返回一个可变的 ref 对象，其 .current 属性被初始化为传入的参数（initialValue）。返回的 ref 对象在组件的整个生命周期内保持不变。