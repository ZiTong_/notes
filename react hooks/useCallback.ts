import * as React from "react"

class App {
  render() {
    return <div>
      <SomeComponent style={{ fontSize: 14 }} doSomething={ () => { console.log('do something') }}  />
    </div>
  }
}

//一旦 App 组件的 props 或者状态改变了就会触发重渲染，即使跟 SomeComponent 组件不相关，
//由于每次 render 都会产生新的 style 和 doSomething（因为重新render前后， style 和 doSomething分别指向了不同的引用），
//所以会导致 SomeComponent 重新渲染，倘若 SomeComponent 是一个大型的组件树，
//这样的 Virtual Dom 的比较显然是很浪费的，解决的办法也很简单，将参数抽离成变量。

/**
 * 类组件
 * @type {Object}
 */
const fontSizeStyle = { fontSize: 14 }

class App {
  doSomething = () => {
    console.log('do something');
  }
  render() {
    return <div>
      <SomeComponent style={fontSizeStyle} doSomething={ this.doSomething }  />
    </div>
  }
}

/**
 * 函数式组件
 * @type {[type]}
 */
const App = () => {
  const handleClick = () => {
    console.log('Click happened');
  }
  return <SomeComponent onClick={handleClick}>Click Me</SomeComponent>;
}

//一版把函数式组件理解为class组件render函数的语法糖，所以每次重新渲染的时候，
//函数式组件内部所有的代码都会重新执行一遍。所以上述代码中每次render，handleClick都会是一个新的引用，
//所以也就是说传递给SomeComponent组件的props.onClick一直在变(因为每次都是一个新的引用)，
//所以才会说这种情况下，函数组件在每次渲染的时候如果有传递函数的话都会重渲染子组件。

/**
 * 函数时组件
 * useCallback
 * @type {[type]}
 */
const App = () => {
  const memoizedHandleClick = React.useCallback(() => {
    console.log('Click happened')
  }, []) // 空数组代表无论什么情况下该函数都不会发生改变
  return <SomeComponent onClick={memoizedHandleClick}>Click Me</SomeComponent>;
}

//第二个参数传入一个数组，数组中的每一项一旦值或者引用发生改变，
//useCallback 就会重新返回一个新的记忆函数提供给后面进行渲染。
//这样只要子组件继承了 PureComponent 或者使用 React.memo 就可以有效避免不必要的 VDOM 渲染。