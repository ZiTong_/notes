```js
class Event{
  constructor(){
    super()
    this.onceEmiter = {}
    this.eventEmiter = {}
  }

  on(type, cb){
    if(!type || !cb || cb !== 'function'){
      return false
    }
    this.onceEmiter[type] = this.onceEmiter[type] || []
    this.eventEmiter[type] = this.eventEmiter[type] || []
    this.onceEmiter[type].push(cb)
    this.eventEmiter[type].push(cb)
  }

  emit(type){
    if(!type){
      return false
    }
    this.eventEmiter[type] && this.eventEmiter[type].forEach(cb => {
      cb.apply(this, [...arguments].slice(1))
    })
    this.onceEmiter[type] && this.onceEmiter[type].forEach(cb => {
      cb.apply(this, [...arguments].slice(1))
    })
    delete this.onceEmiter[type]
  }

  once(type, cb){
    if(!type || !cb || cb !== 'function'){
      return false
    }
    this.onceEmiter[type] = this.onceEmiter[type] || []
    this.onceEmiter[type].push(cb)
  }

  off(type){
    if(!type){
      return false
    }
    delete this.onceEmiter[type]
    delete this.eventEmiter[type]
  }
}
```