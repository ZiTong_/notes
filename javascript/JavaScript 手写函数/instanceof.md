## instanceof

基于原型链来判断
```js
const instanceof = (a, b) => {
  if (typeof a !== "object" && typeof b !== "object") {
    return false
  }
  let proto = Object.getProtoTypeOf(a)
  let prototype = b.__proto__
  while (proto !== null) {
    if(proto === prototype){
      proto = null
      return true
    }
    proto = proto.__proto__
  }
}
```
