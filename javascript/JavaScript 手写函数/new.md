```js
function Person (name){
  this.name = name
  this.sayName = function(){
    return this.name
  }
}
const p1 = new Person('alex')
p1.sayName()

new Person('alex') => {
  const obj = {}
  obj.__proto__ = Person.prototype
  const res = Person.call(obj, 'alex')
  return typeof res === 'object' ? res : obj
}
```

1. 创建一个新的对象
2. 新对象的原型指定构造函数的原型对象
3. 执行构造,函数中的 this 指向新的实例对象
4. 返回新的对象(除非构造函数中返回的是"无原型")

```js
function _new(fn, ...args) {
  const obj = Object.create(fn.prototype)
  const ret = fn.apply(obj, args)
  return ret instanceof Object ? ret : obj
}
```

// 圣杯继承

```js
const inherit = function(target, origin){
  const F = (){}
  return function(target, origin){
    F.prototype = origin.prototype
    target.prototype = new F()
    target.prototype.constructor = target
  }
}

```
利用中间的匿名函数做一层中间层、这样修改实例的原型不会污染所继承的原型
