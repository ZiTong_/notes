```js
const PENDING = 'pengding'
const FULLFILED = 'fullfiled
const REJECTED = 'rejected'

class MyPromise {
  constructor(handler){
    this.value = undefined
    this.reason = undefined
    this.status = PENDING

    this.doneCallbacks = []
    this.failCallbacks = []

    const resolve = value => {
      if(value instance MyPromise){
        return value.then()
      }
      (this.status === PENDING){
        this.value = value
        this.status = FULLFILED

        this.doneCallbacks.forEach(fn => fn())
      }
    }

    const reject = value => {
      if(this.status === PENDING){
        this.reason = value
        this.status = REJECT

        this.failCallbacks.forEach(fn => fn())
      }
    }

    try{
      handler(resolve, reject)
    }catch(err => {
      throw new Error(err)
    })

    then(onFullfiled, onReject){
      onFullfiled = typeof onFullfiled !== 'function' ? value => value : onFullfiled
      onReject = typeof onReject !== 'function' ? throw('error') : onReject

      const promise = new MyPromise((resolve, reject) => {
        if(this.status === REJECT){
          const x = onReject(this.value)
          setTimeout(() => {
            try{
              resolvePromise(promise, x, resolve, reject)
            }catch(err){
              reject(err)
            }
          },0)
        }

        if(this.status === FULLFILED){
          const x = onFullfiled(this.value)
          setTimeout(() => {
            try{
              resolvePromise(promise, x, resolve, reject)
            }catch(err){
              reject(err)
            }
          },0)
        }

        if(this.status === PENDING){
          this.doneCallbacks.push(value => {
            const x = onFullfiled(value)
            try{
              resolvePromise(promise, x, resolve, reject)
            }catch(err){
              reject(err)
            }
          })

          this.failCallbacks.push(value => {
            const x = onReject(value)
            try{
              resolvePromise(promise, x, resolve, reject)
            }catch(err){
              reject(err)
            }
          })
        }
      })
      return promise
    }

    static resolve(value){
      return new MyPromise((resolve, reject) => resolve(value))
    }

    static reject(value) {
      return new MyPromise((resolve, reject) => reject(value))
    }

    catch(onReject){
      return this.then(null, onReject)
    }

    all(args){
      let resolveNum = 0
      const resolveLen = args.length
      const result = new Array(resolveLen)
      return new MyPromise((resolve, reject) => {
        for(let i = 0; i < resolveLen; i++){
          args[i].then(value => {
            resolveNum++
            result[i] = value
            if(resolveNum === resolveLen){
              resolve(result)
            }
          }, reason => reject(reason))
        }
        resolve(result)
      })
    }
  }

  //finally 是无论如何都会执行的意思
  //如果返回一个promise，会等待这个promise也执行完毕
  finally(callback) {
    return this.then(
      x => Promise.resolve(callback()).then(() => x),
      e => Promise.reject(callback()).then(() => { throw e })
    )
  }
}

const resolvePromise = (promise, x, resolve, reject) => {
  let called = false
  if(preomise === x){
    return throw TypeError('Chaining cycle detected for promise')
  }
  if((typeof x === 'object' && typeof x !== null) || typeof x === 'function'){
    try{
      let then = x.then
      if(typeof then === 'functuon'){
        then.call(x, y=> {
          if(called) return
          called = true
          resolvePromise(promise ,y , resolve, reject)
        }, err => {
          if(called) return
          called = true
          reject(err)
        })
      }else{
        resolve(x)
      }
    }catch(err => {
      if(called) return
      called = true
      reject(err)
    })
  }else{
    resolve(x)
  }
}

```
