### Typeof
```js
typeof 1 'number'
typeof NAN 'number'
typeof false 'boolean'
typeof '1' 'string'
typeof undefined 'undefined'
typeof 123n 'bigint'
typeof Symbol() 'symbol'
typeof {} 'object'
typeof [] 'object'
typeof function fun(){} 'function'
typeof null 'object' 
```
>JavaScript 中的值是由一个表示类型的标签和实际数据值表示的.
对象的类型标签是 0.由于 null 代表的是空指针(大多数平台下值为 0x00),
因此，null 的类型标签是 0,typeof null 也因此返回 "object"

js 在底层存储变量的时候，会在变量的机器码的低位1-3位存储其类型信息, 共有32位  

000：对象  
010：浮点数  
100：字符串  
110：布尔  
1：整数

null: 所有的机器码为0.  
undefined: -2^30整数表示.  
typeof null 因为null的机器码为0,所以直接当成对象处理 直接返回'object'.

### Object.prototype.toString().call()

```js
Object.prototype.toString.call(new Date())    [object, Date]  
Object.prototype.toString.call([])            [objedt, Array]  
Object.prototype.toString.call(/reg/ig)       [object, RegExp]
Object.prototype.toString.call(Symbol(1))     [object Symbol]
Object.prototype.toString.call(null)          [object Null]
Object.prototype.toString.call(undefined)     [object Undefined]
Object.prototype.toString.call(function(){})  [object Function]
```

### instanceOf
>符用于检测构造函数的 prototype 属性是否出现在某个实例对象的原型链上
```js
[] instanceof Array  true
{} instanceof Obejct true
show function(){} instanceof Function true
```