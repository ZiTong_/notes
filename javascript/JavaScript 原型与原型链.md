**原型与原型链**

1. prototype ：每个函数都会有这个属性，这里强调，是函数，普通对象是没有这个属性的（这里为什么说普通对象呢，因为JS里面，一切皆为对象，所以这里的普通对象不包括函数对象）。它是构造函数的原型对象；
2. __proto__ ：每个对象都有这个属性,，这里强调，是对象，同样，因为函数也是对象，所以函数也有这个属性。它指向构造函数的原型对象；
3. constructor ：这是原型对象上的一个指向构造函数的属性。

```js
function Pig(name: string, age: number){
  this.name = name
  this.age = age
}

// 创建Pig实例
const Pepig = new Pig('Pepig', 18)

// 在实例化的时候，prototype上的属性会作为原型对象赋值给实例, 也就是Pepig原型
Pepig.__proto__ === Pig.prototype    // true

// Pig是一个函数对象, 它是Function对象的一个实例 Funtcion的原型对象又指向Object对象
Pig.__proto__ === Function.prototype  // true

Function.prototype.__proto__ === Object.prototype // true

Object.prototype.__proto__ === null // true

// 原型对象上的constructor指向构造函数本身
Pig.prototype.constructor = Pig

```