**JavaScript 语言中的 this**

1. 由于其运行期绑定的特性，JavaScript 中的 this 含义要丰富得多，它可以是全局对象、当前对象或者任意对象，这完全取决于函数的调用方式。JavaScript 中函数的调用有以下几种方式：作为对象方法调用，作为函数调用，作为构造函数调用，和使用 apply 或 call 调用。下面我们将按照调用方式的不同，分别讨论 this 的含义。
```
1.作为对象方法调用

var point = { 
	x : 0, 
	y : 0, 
	moveTo : function(x, y) { 
		this.x = this.x + x
		this.y = this.y + y 
	} 
}

point.moveTo(1, 1)//this 绑定到当前对象，即 point 对象
``` 

```
2.作为函数调用

function makeNoSense(x) { 
	this.x = x
} 
 
makeNoSense(5); 
x; // x 已经成为一个值为 5 的全局变量

函数被调用时，this 被绑定到全局对象，接下来执行赋值语句，相当于隐式的声明了一个全局变量，
这显然不是调用者希望的。

2.1对于内部函数，即声明在另外一个函数体内的函数
var point = { 
	x : 0, 
	y : 0, 
	moveTo : function(x, y) { 
		// 内部函数
		var moveX = function(x) { 
			this.x = x //this 绑定到了哪里？
		}
		// 内部函数
		var moveY = function(y) { 
		this.y = y //this 绑定到了哪里？
		}
	
		moveX(x) 
		moveY(y)
	} 
}
point.moveTo(1, 1); 
point.x; //==>0 
point.y; //==>0 
x; //==>1 
y; //==>1

这属于 JavaScript 的设计缺陷，正确的设计方式是内部函数的 this 应该绑定到其外层函数对应的对象上，为了规避这一设计缺陷，聪明的 JavaScript 程序员想出了变量替代的方法，约定俗成，该变量一般被命名为 that
```

```
3.作为构造函数调用

function Point(x, y){ 
	this.x = x 
	this.y = y
}
如果调用正确，this 绑定到新创建的对象上。


Apply(数组)、call(正常参数)

function Point(x, y){ 
	this.x = x; 
	this.y = y; 
	this.moveTo = function(x, y){ 
		this.x = x; 
		this.y = y; 
	} 
} 
 
var p1 = new Point(0, 0)
var p2 = {x: 0, y: 0}
p1.moveTo(1, 1)
p1.moveTo.apply(p2, [10, 10])


在上面的例子中，我们使用构造函数生成了一个对象 p1，该对象同时具有 moveTo 方法；使用对象字面量创建了另一个对象 p2，我们看到使用 apply 可以将 p1 的方法应用到 p2 上，这时候 this 也被绑定到对象 p2 上。另一个方法 call 也具备同样功能，不同的是最后的参数不是作为一个数组统一传入，而是分开传入的。
```