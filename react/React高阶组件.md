
```jsx
export function ppHOC(WrappedComponent:React.ComponentType):React.ComponentType{
  return class PP extends React.Component{
    public render(){
      return <WrappedComponent {...this.props} />
    }
  }
}

// just like this
<WrappedComponent {...this.props}/> ===  React.createElement(WrappedComponent, this.props, null)

```

// 通过 Refs 访问到组件实例
```jsx
export function refsHOC(WrappedComponent:React.ComponentType):React.ComponentType {
  // tslint:disable-next-line:max-classes-per-file
  return class RefsHOC extends React.Component {
    public proc(wrappedComponentInstance:any) {
      wrappedComponentInstance.method()
    }
  
    public render() {
      const props = Object.assign({}, this.props, {ref: this.proc.bind(this)})
      return <WrappedComponent {...props}/>
    }
  }
}

```

```jsx
export function psHOC(WrappedComponent:React.ComponentType):React.ComponentType{
  // tslint:disable-next-line:max-classes-per-file
  return class PS extends React.Component<any, { name:string }>{
    constructor(props:any) {
      super(props)
      this.state = {
        name: ''
      }
      this.onNameChange = this.onNameChange.bind(this)
    }
    public onNameChange(event:any) {
      this.setState({
        name: event.target.value
      })
    }
    public render() {
      const newProps = {
        name: {
          onChange: this.onNameChange,
          value: this.state.name
        }
      }
      return <WrappedComponent {...this.props} {...newProps}/>
    }
  }
}
```