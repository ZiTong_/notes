import * as React from "react"

class EventHandle extends React.Component {

	constructor(props){
		super(props)
		// this.handleClick = this.handleClick.bind(this)
	}

	hanledClick = () => {
	}

	render(){
		return 
			// <div onClick={this.handleClick}>Hello World</div>
		<div onClick={() => this.handleClick}>Hello World</div>
	}
}

/**
React中事件处理
Q:1. this.handleClick
此时这中写法，将this.handleClick这个函数当做一个变量赋值给onClick，
通过onClick来调用，这里就会丢失上下文，所以this会丢失，可以看函数如何在js中运行的
A:1. this.handleClick = this.handleClick.bind(this)
  2. handleClick = () => {} 
Q: 1.handleClick() || this.handleClick()
A: handleClick()无法找到上下文，this丢失。this.handleClick()，会立即执行，不需要触发，不符合事件处理
Q: 1.() => {this.handleClick} || (e) => this.handleClick(id, e) || this.handleClick.bind(this,id)
A： 箭头函数自动绑定上下文，有额外的渲染，事件函数需在参数后面，bind隐式传递
*/